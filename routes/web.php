<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('web_chat');
});
Route::get('/customer', function () {
    return view('web_chat_customer');
});

//Auth::routes();

Route::post('/get_room', 'HomeController@getRoom');
Route::get('/get_messages', 'HomeController@getMessages');
Route::get('/get_message', 'HomeController@getMessage');
Route::post('/store_message', 'HomeController@storeMessage');
Route::get('/home', 'HomeController@index')->name('home');


Route::get('/get_user', 'HomeController@getUser');
Route::get('/get_support', 'HomeController@getSupport');
