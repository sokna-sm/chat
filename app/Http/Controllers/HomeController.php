<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{

    protected $support = null;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
        $this->support = DB::table('users')->where('firebase_id', 'IhVGsl59lfYtuLEGppXiHZQ3TTU2')->first();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRoom(Request $request)
    {
        $authId = $this->support->firebase_id;
        $roomIds = $request->room_id;
        $destIds = $request->dest_id;
//        dd($roomIds, $destIds);
//        $data = DB::table('chat_item')
//            ->whereIn('chat_list_id', $roomIds)
//            ->whereNotNull('created_at')
//            ->select([
//                'chat_list_id'
//            ])
//            ->groupBy(trim('chat_list_id'))
//            ->limit(100)
//            ->get();



        $roomFormat = [
            'id' => '',
            'active' => 0,
            'online' => 0,
            'avatar' => '',
            'total_unread' => 0,
            'display_name' => ' => sender_name != auth'
        ];
        $room= [];
        foreach ($roomIds as $key => $item) {
            $displayName =  DB
                ::table('chat_item')
                ->where('chat_list_id', '=', $item)
                ->where('sender_id', '!=', $authId)
                ->whereNotNull('created_at')
                ->orderBy('created_at', 'desc')
                ->first();
            $lastMessage =  DB
                ::table('chat_item')
                ->where('chat_list_id', '=', $item)
                ->whereNotNull('created_at')
                ->orderBy('created_at', 'desc')
                ->first();
            if ($displayName) {
                $data = [
                    'chat_list_id' => $displayName->chat_list_id,
                    'active' => 0,
                    'online' => 0,
                    'avatar' => '',
                    'total_unread' => 0,
                    'display_name' => $displayName->sender_name,
                    'sender_id' => $displayName->sender_id,
                    'last_message' => @$lastMessage->message,
                    'destination' => '',

                ];
                foreach ($destIds[$key] as $index => $value) {
                    if ($value != $authId) {
                        $destination = DB::table('users')->where('firebase_id', $value)->first();
                        $data['avatar'] = $destination->image_url;
                        $data['destination'] = $destination;
                    }
                }

                array_push($room, $data);
            }
        }

//        $chatListId = 'R5JN5w22WXRKH2k0MAk42MmIM';
//
//        $displayName =  DB
//            ::table('chat_item')
//            ->where('chat_list_id', '=', $chatListId)
//            ->where('sender_id', '!=', $senderId)
//            ->whereNotNull('created_at')
//            ->orderBy('created_at', 'desc')
//            ->first();
//
//        dd($displayName);

        return response()->json($room);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMessages(Request $request)
    {
        $chatId = $request->room_id;
        $data = DB::table('chat_item')
            ->where('chat_list_id', '=', $chatId)
            ->whereNotNull('created_at')
            ->select([
                '*'
            ])
            ->get();

        foreach ($data as $key => $value) {
            $value->message_object = json_decode($value->message_object, JSON_UNESCAPED_UNICODE);
        }
        return response()->json($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMessage(Request $request)
    {
        $chatId = $request->chat_id;
        $data = DB::table('chat_item')
            ->where('chat_id', '=', $chatId)
            ->whereNotNull('created_at')
            ->select([
                '*'
            ])
            ->first();

        if ($data){
            $data->message_object = json_decode($data->message_object, JSON_UNESCAPED_UNICODE);

        } else {
            sleep(1);
            $chatId = $request->chat_id;
            $data = DB::table('chat_item')
                ->where('chat_id', '=', $chatId)
                ->whereNotNull('created_at')
                ->select([
                    '*'
                ])
                ->first();
            $data->message_object = json_decode($data->message_object, JSON_UNESCAPED_UNICODE);

        }
        return response()->json($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeMessage(Request $request)
    {
        $param = $request->all();
        $param['message_object'] = json_encode($param['message_object'], JSON_UNESCAPED_UNICODE);
        $param['created_at'] = (string)Carbon::now();
        $param['updated_at'] = (string)Carbon::now();
        $param['date'] = (string)Carbon::now();
        $param['do_not_show_to'] = 0;
        DB::table('chat_item')->insert($param);
        $data = DB::table('chat_item')->where('chat_id', $param['chat_id'])->first();
        $data->message_object = json_decode($data->message_object, JSON_UNESCAPED_UNICODE);
        return response()->json($data);
    }


    ///////////////////////////////
    public function getUser()
    {
        $data = DB
            ::table('users')
            ->whereNotIn('firebase_id', [$this->support->firebase_id])
            ->limit(100)
            ->get();
        return response()->json($data);

    }

    public function getSupport()
    {
        $data = DB
            ::table('users')
            ->whereIn('firebase_id', [$this->support->firebase_id])
            ->limit(100)
            ->first();
        return response()->json($data);

    }
}
