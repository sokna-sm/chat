import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/storage';

const config = {
	
    // apiKey: "AIzaSyBWGV-NJEHcEj8JWoN6chD1PHDWSaVQnbo",
    // authDomain: "nothing-19494.firebaseapp.com",
    // projectId: "nothing-19494",
    // storageBucket: "nothing-19494.appspot.com",
    // messagingSenderId: "336123185734",
    // appId: "1:336123185734:web:25608be93bce68e2d45847",
    // measurementId: "G-WTGVPPV8JN"

        apiKey: "AIzaSyA3TLdEYBqaLgPDVmRHIsYsDIEA0uM4xfQ",
        authDomain: "mouyapp.firebaseapp.com",
        databaseURL: "https://mouyapp.firebaseio.com",
        projectId: "mouyapp",
        storageBucket: "stayfit",
        messagingSenderId: "869231360670",
        appId: "1:869231360670:web:1c4ddb0af9c6c35c05d98f"
};

// Get a Firestore instance
export const db = firebase
    .initializeApp(config)
    .firestore()

export const firestorage = firebase.storage();
// Export types that exists in Firestore
// This is not always necessary, but it's used in other examples
const { Timestamp, GeoPoint } = firebase.firestore
export { Timestamp, GeoPoint }

// if using Firebase JS SDK < 5.8.0
db.settings({ timestampsInSnapshots: true })
