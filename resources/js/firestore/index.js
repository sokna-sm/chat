import * as app from 'firebase/app'
import 'firebase/firestore'
import 'firebase/database'
import 'firebase/storage'

// const config =
// 	process.env.NODE_ENV === 'development'
// 		? JSON.parse(process.env.VUE_APP_FIREBASE_CONFIG)
// 		: JSON.parse(process.env.VUE_APP_FIREBASE_CONFIG_PUBLIC)

var config = {
  
    apiKey: "AIzaSyA3TLdEYBqaLgPDVmRHIsYsDIEA0uM4xfQ",
    authDomain: "mouyapp.firebaseapp.com",
    databaseURL: "https://mouyapp.firebaseio.com",
    projectId: "mouyapp",
    storageBucket: "stayfit",
    messagingSenderId: "869231360670",
    appId: "1:869231360670:web:1c4ddb0af9c6c35c05d98f"
};

app.initializeApp(config)

export const firebase = app
export const db = app.firestore()
export const storageRef = app.storage().ref()

export const usersRef = db.collection('users')
export const roomsRef = db.collection('chatRooms')

export const filesRef = storageRef.child('files')

export const dbTimestamp = firebase.firestore.FieldValue.serverTimestamp()
export const deleteDbField = firebase.firestore.FieldValue.delete()
