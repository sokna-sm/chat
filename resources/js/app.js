/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import { firestorePlugin } from 'vuefire'
Vue.use(firestorePlugin);
Vue.use(require('vue-moment'));

import Vue from "vue";
import UUID from "vue-uuid";
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
Vue.use(UUID);
// Vue.use(BootstrapVue)
//
// import 'bootstrap/dist/css/bootstrap.css'
// import 'bootstrap-vue/dist/bootstrap-vue.css'
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('chat-box', require('./components/ChatBox.vue').default);
Vue.component('chat-box-customer', require('./components/ChatBoxCustomer.vue').default);
Vue.component('upload-file', require('./components/UploadFile.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// import firestore from './firestore/index.js'
// import firebase from 'firebase/app'
// import 'firebase/firestore'
//
// const db = firebase.initializeApp({ projectId: 'MY PROJECT ID' }).firestore()

const app = new Vue({
    el: '#app',
});
