@extends('layouts.chat')

@section('content')
    <div class="container">
        <br>
        <a href="/">Support</a>
        <hr>
        <div class="row">
            <div class="col-sm-12">
                <chat-box-customer></chat-box-customer>
            </div>
        </div>
    </div>
@endsection